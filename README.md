# Custom NGINX Image in GitLab Container Registry

This repository contains the code for a custom NGINX Docker image that has been pushed to the GitLab Container Registry.

## Dockerfile

The `Dockerfile` in this repository builds a custom NGINX image with some additional configuration and modules. It starts with the official `nginx` base image and adds the following:

- Custom configuration file: `nginx.conf`
- Contents of index.html file

## Building the Image

To build the image, run the following command:
docker build -t <registry>/<namespace>/<image-name>:<tag> .


Replace `<registry>`, `<namespace>`, `<image-name>`, and `<tag>` with the appropriate values for your GitLab Container Registry.

For example, if your GitLab project is hosted at `gitlab.com` and your namespace is `my-namespace`, you can build the image with the following command:

docker build -t registry.gitlab.com/my-namespace/my-nginx-image:latest .


## Pushing to the GitLab Container Registry

To push the image to the GitLab Container Registry, you need to authenticate first by running:

docker login registry.gitlab.com

Then, push the image with the following command:

docker push <registry>/<namespace>/<image-name>:<tag>

For example:

docker push registry.gitlab.com/my-namespace/my-nginx-image:latest


## Using the Custom NGINX Image

To use the custom NGINX image, you can pull it from the GitLab Container Registry using the following command:


## Using the Custom NGINX Image

To use the custom NGINX image, you can pull it from the GitLab Container Registry using the following command:

docker pull <registry>/<namespace>/<image-name>:<tag>

For example:

docker pull registry.gitlab.com/my-namespace/my-nginx-image:latest

You can then use the image to start a container as you would with any other NGINX image.

